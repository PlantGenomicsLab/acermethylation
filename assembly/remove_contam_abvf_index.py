import sys
from Bio import SeqIO
fastq_file = "acne_superaccuracy_all.fastq"
remove_file = "acne_contam_readids_abvf_index_uniq.txt"
result_file = "acne_abvf_filtered.fastq"
with open(result_file, "w") as a, open(remove_file, "r") as b:
	reader = b.read().splitlines()
	for seq in SeqIO.parse(fastq_file, "fastq"):
		if seq.id not in reader:
			SeqIO.write(seq, a, "fastq")



