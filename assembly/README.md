<h1>Genome Assembly</h1>

<h3>Filtering</h3>

<h4>Centrifuge</h4>

Masurca prefers raw Illumina reads, but we filtered the Nanopore for contaminants using Centrifuge. Be careful to not filter out plastid sequences  
The abvf index here includes archaea, bacteri, bacteria, and fungi  

centrifuge -p 25 -x abvf --report-file centrifuge_acne_abvf_index_report.tsv --quiet --min-hitlen 50 -q acne_superaccuracy_all.fastq  

A custom script (remove_contam_abvf_index.py, linked above) was used to filter based on the centrifuge output (here, as centrifuge_3636036.out):  
module load biopython/1.70  
grep -vw "unclassified" centrifuge_3636036.out > acne_contam_reads_abvf_index.txt  
awk NF=1 acne_contam_reads_abvf_index.txt > acne_contam_readids_abvf_index.txt  
sort -u acne_contam_readids_abvf_index.txt > acne_contam_readids_abvf_index_uniq.txt  
python remove_contam_abvf_index.py  

<h4>NanoPlot</h4>

NanoPlot was used to evaluate the filtered reads  

NanoPlot -o nanoplot_out -t 30 --fastq acne_abvf_filtered.fastq  

<h3>Contig Assembly</h3>

<h4>Masurca</h4>
export PATH=$PATH:/core/labs/Wegrzyn/local_software/MaSuRCA-4.0.3/bin:/core/labs/Wegrzyn/local_software/MaSuRCA-4.0.3/lib:  
LD_LIBRARY_PATH=/core/labs/Wegrzyn/local_software/MaSuRCA-4.0.3/lib  

Run the first command which uses the configurations to generate the assemble.sh file  
/core/labs/Wegrzyn/local_software/MaSuRCA-4.0.3/bin/masurca config.txt  

Then run the resulting script  
./assemble.sh  

<h3>Haplotig filtering</h3>

<h4>Purge Haplotigs - Not Used</h4>

Purge Haplotigs was tested at this point, but was NOT used, give that the Masurca assemblies had a very low duplication rate and purging haplotypes caused the single copy completeness score to drop  

minimap2 -ax map-ont -t 20 ../masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy/nanofilt/acne_abvf_5k_filtered.fastq --secondary=no | samtools sort -@ 20 -m 15G -o acne_masurca_minimap.sorted.bam -T $HOME/reads.tmp  

/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs readhist -b acne_masurca_minimap.sorted.bam -g ../masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta -t 25  

Parameters selected by viewing plot from previous command  
/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs contigcov -i acne_masurca_minimap.sorted.bam.gencov -l 7 -m 60 -h 145  

/isg/shared/apps/purge_haplotigs/1.0/bin/purge_haplotigs purge -g ../masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta -c coverage_stats.csv -b acne_masurca_minimap.sorted.bam -t 10  


<h3>Polishing</h3>

<h4>Pilon</h4>

The Masurca assembly was used as input without purging haplotypes  

Prepare a Bowtie alignment with Illumina trimmed reads  
bowtie2-build --threads 16 /core/projects/EBP/Wegrzyn/acer/acne/assembly/masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta acnegenomebowtieidx  
bowtie2 -p 36 -x acnegenomebowtieidx -1 /core/projects/EBP/Wegrzyn/acer/acne/reads/illumina/trimmed_AN-B_S55_L002_R1.fastq -2 /core/projects/EBP/Wegrzyn/acer/acne/reads/illumina/trimmed_AN-B_S55_L002_R2.fastq -S acnegenomebowtie.sam  
samtools view -@ 36 -b acnegenomebowtie.sam | samtools sort -@ 36 -o acnegenomebowtie.sorted.bam  
samtools index acnegenomebowtie.sorted.bam  

Use the sorted, indexed alignment as input with the Masurca draft genome   
java -jar -Xmx400G /isg/shared/apps/pilon/1.24/pilon-1.24.jar  --genome /core/projects/EBP/Wegrzyn/acer/acne/assembly/masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta  --bam /core/projects/EBP/Wegrzyn/acer/acne/alignments-self/bowtie/acnegenomebowtie.sorted.bam --output acne_pilon --outdir pilon_o --changes --tracks  

<h4>Medaka - Not Used</h4>

Masurca/Medaka and Masurca/Medaka/PurgeHaplotig workflows were tested but resulted in lower Quast and Busco scores  
r941_prom_sup_g507 is the name of the ONT basecalling model used to rebasecall  

medaka_consensus -i /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy/nanofilt/acne_abvf_5k_filtered.fastq -d ../masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta  -o acne_masurca_medaka -t 31 -m r941_prom_sup_g507  


<h3>Scaffolding</h3>  

Ragtag was used to scaffold the polished Masurca assembly using the original Acer genomes  

ragtag.py scaffold /labs/Wegrzyn/AcerGenomes/acne/genome/acne_genome.fasta ../masurca/CA.mr.41.17.15.0.02/primary.genome.scf.fasta   

<h3>Assembly Metrics</h3>

<h4>Quast</h4>

input="ragtag_output/ragtag.scaffold.fasta"  
python /isg/shared/apps/quast/5.0.2/quast.py "$input" -o quast_o -t 9  

<h4>Busco</h4>

Both viridiplantae_odb10 and embryophyta were used  
busco -i ragtag_output/ragtag.scaffold.fasta -o busco_v -l viridiplantae_odb10 -m geno -c 15  
