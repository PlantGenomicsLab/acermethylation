#!/bin/bash
#SBATCH --job-name=gfacs
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 2
#SBATCH --partition=himem2
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o gfacs_%j.out
#SBATCH -e gfacs_%j.err

hostname
echo "\nStart time:"
date

module load perl/5.28.1

genome="/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta"
alignment="acne2_gfacs_filter_gene_table_geneids_removed_and_added.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--no-processing \
--statistics \
--get-protein-fasta \
--get-fasta \
--create-gff3 \
--create-gtf \
--fasta "$genome" \
-O gfacs_o \
"$alignment"


echo "\nEnd time:"
date

