#!/bin/bash
#SBATCH --job-name=gffcompare
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 6
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=80G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o gffcompare_%j.out
#SBATCH -e gffcompare_%j.err

#module load gffcompare

#change the gmap/gfacs gtf output to get this to work
# :%s/Parent=/transcript_id "/g
# :%s/mrna1/mrna1";/g

#/core/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acne -s /core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta -r ../gfacs_filter_masked/final_o/out.gtf ../../alignments/gmap/gfacs_o/out_transcriptid.gtf

/core/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acnebraker -s /core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta -r ../braker/workingdirmasked2/braker1/augustus.hints.gtf ../../alignments/gmap/gfacs_o/out_transcriptid.gtf
