#!/bin/bash
#SBATCH --job-name=remove_geneids
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=50G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o remove_geneids_%j.out
#SBATCH -e remove_geneids_%j.err

hostname
echo "\nStart time:"
date

### These lines are just to provide one example, you will need to identify which genes or transcripts you want to replace or add and modify the files and lists of ids accordingly

### for the replacement transcripts, remove the geneids from the main list (replacements added back as genetables in the final command)
#grep -Ff k/k_transcripts_matching_refmultis_gfacs_multi/k_transcripts_matching_refmultis_gfacs_multi_geneids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/\..*$//g' > k_transcripts_matching_refmultis_gfacs_multi_geneids_to_remove.txt

#grep -Ff k/k_transcripts_matching_refmonos_gfacs_multi/k_transcripts_matching_refmonos_gfacs_multi_geneids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/\..*$//g' > k_transcripts_matching_refmonos_gfacs_multi_geneids_to_remove.txt

#grep -Ff k/k_transcripts_matching_refmultis_gfacs_mono/k_transcripts_matching_refmultis_gfacs_mono_geneids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/\..*$//g' > k_transcripts_matching_refmultis_gfacs_mono_geneids_to_remove.txt

#cat k_transcripts_matching_refmultis_gfacs_multi_geneids_to_remove.txt k_transcripts_matching_refmonos_gfacs_multi_geneids_to_remove.txt k_transcripts_matching_refmultis_gfacs_mono_geneids_to_remove.txt | sed 's/^/ID=/g' | sed 's/|.*$/;/g' > geneids_to_remove.txt

#grep 'gene' ../gfacs_filter_masked/final_o/gene_table.txt | cut -f6 | sed 's/;.*$//g' | sed 's/$/;/g' > geneids.txt

#grep -vFf geneids_to_remove.txt geneids.txt | sed 's/;//g' | sed 's/\..*$//g' > geneids_to_keep.txt

#python filtergFACsGeneTable.py --table ../gfacs_filter_masked/final_o/gene_table.txt --tablePath . --idList geneids_to_keep.txt --idPath . --out gfacs_filter_gene_table_geneids_removed.txt

### for the previously filtered gene models with transcript support, we want to add them back, but use the gene models for the  monoexomics.  Multiexonics can use the transcript.
#grep -Ff u/u_filtered_transcripts_matching_brakermonos_gfacs_mono/u_filtered_transcripts_matching_brakermonos_gfacs_mono_geneids_to_remove.txt acnebraker.tracking | cut -f3 | sed 's/\..*$//g' | sed 's/^/ID=/g' | sed 's/|.*$//g' > u_filteredtranscripts_matching_refmonos_gfacs_mono_geneids_to_add.txt
# i manually removed 3 from the output above as they were isoforms I didn't mean to capture

#python filtergFACsGeneTable.py --table ../braker/workingdirmasked2/braker1/gfacs_o/gene_table.txt --tablePath . --idList u_filteredtranscripts_matching_refmonos_gfacs_mono_geneids_to_add.txt --idPath . --out u_to_add_gene_table.txt

cat gfacs_filter_gene_table_geneids_removed.txt k/k_transcripts_matching_refmultis_gfacs_multi/gene_table.txt k/k_transcripts_matching_refmonos_gfacs_multi/gene_table.txt k/k_transcripts_matching_refmultis_gfacs_mono/gene_table.txt k/k_filtered_transcripts_matching_brakermonos_gfacs_multi/gene_table.txt k/k_filtered_transcripts_matching_brakermonos_gfacs_mono/gene_table.txt k/k_filtered_transcripts_matching_brakermultis_gfacs_multi/gene_table.txt u/u_filtered_transcripts_matching_brakermonos_gfacs_multi/gene_table.txt  u/u_filtered_transcripts_matching_brakermultis_gfacs_multi/gene_table.txt u_to_add_gene_table.txt > acne2_gfacs_filter_gene_table_geneids_removed_and_added.txt

echo "\nEnd time:"
date

