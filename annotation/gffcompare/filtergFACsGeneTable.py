#!/usr/bin/env python
import re
import argparse
import os

parser = argparse.ArgumentParser(
     prog='filtergFACsGeneTable.py',
     usage='''python filtergFACsGeneTable.py --table [gene table from gFACs] --tablePath [Path of gene_table file] --idList [name of list file] --idPath [path of the list file] --out [name of output gene_table file]''',
     description='''This program filters out specific genes from a gFACs gene_table file, given the gene_table.txt file and a list of sequences saved in a text file''')
parser.add_argument('--table', type=str, help='The name of the fasta file', required=True)
parser.add_argument('--tablePath', type=str, help='The path of the fasta file', required=False)
parser.add_argument('--idList', type=str, help='name of the list file that contains name of sequences', required=True)
parser.add_argument('--idPath', type=str, help='path of list file', required=False)
parser.add_argument('--out', type=str, help='name of output fasta file', required=True)

args=parser.parse_args()
tablepath=args.tablePath
table=args.table
listName=args.idList
listPath=args.idPath
output=args.out

if tablepath==None:
    tableFile=table
else:
    tableFile=os.path.join(tablepath, table)

if listPath==None:
    listFile=listName
else:
    listFile=os.path.join(listPath, listName)



# open the list of words to search for
list_file = open(listFile)

search_words = []
in_hash = 0

# loop through the words in the search list
for word in list_file:

    # save each word in an array and strip whitespace
    search_words.append(word.strip())

list_file.close()

# this is where the matching lines will be stored
matches = []

# open the master file
master_file = open(tableFile)
# loop through each line in the master file
for line in master_file:
    # split the current line into array, this allows for us to use the "in" operator to search for exact strings
    current_line = re.split('\s|;|\.',line)
    if re.search( r"^###", line):
        if in_hash == 1:
            in_hash = 0
    # loop through each search word
    for search_word in search_words:
        if search_word in current_line:
            in_hash = 1
            break
    if in_hash == 1 or '###' in current_line:    
        # if found then save the line as we found it in the file
        matches.append(line)

master_file.close()


# create the new file
new_file = open(output, 'w+')

# loop through all of the matched lines
for line in matches:

    # write the current matched line to the new file
    new_file.write(line)

new_file.close()

