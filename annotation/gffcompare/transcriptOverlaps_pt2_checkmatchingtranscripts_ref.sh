#!/bin/bash
#SBATCH --job-name=transcriptOverlaps
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=15G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o transcriptOverlaps_%j.out
#SBATCH -e transcriptOverlaps_%j.err

echo "\nStart time:"
date

# get the gfacs verified braker genes ids
#grep 'gene' k_ref_genes_gfacs_mono/gene_table.txt | sed 's/^.*ID=//g' | sed 's/;.*$/\./g' > k_ref_mono_geneids.txt

#grep 'gene' k_ref_genes_gfacs_multi/gene_table.txt | sed 's/^.*ID=//g' | sed 's/;.*$/\./g' | sed 's/\.1$//g' > k_ref_multi_geneids.txt

# use the ids to find the matching transcript ids
#grep -Ff k_ref_mono_geneids.txt k.tracking | cut -f5 | sed 's/^.*q1://g' | sed 's/.mrna.*$//g' > k_ref_mono_transcriptids.txt

grep -Ff k_ref_multi_geneids.txt k.tracking | cut -f5 | sed 's/^.*q1://g' | sed 's/.mrna.*$//g' > k_ref_multi_transcriptids.txt

# use the transcript ids to find those annotations in the gmap gfacs gene_table
#python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath ../../../alignments/gmap/gfacs_o/ --idList k_ref_mono_transcriptids.txt --idPath . --out k_ref_mono_transcripts_gene_table.txt

python ../filtergFACsGeneTable.py --table gene_table.txt --tablePath ../../../alignments/gmap/gfacs_o/ --idList k_ref_multi_transcriptids.txt --idPath . --out k_ref_multi_transcripts_gene_table.txt

# verify these transcripts
#mkdir k_transcripts_matching_refmonos_gfacs_mono
#sbatch k_transcripts_matching_refmonos_gfacs_mono.sh

#mkdir k_transcripts_matching_refmonos_gfacs_multi
#sbatch k_transcripts_matching_refmonos_gfacs_multi.sh

#mkdir k_transcripts_matching_refmultis_gfacs_mono
sbatch k_transcripts_matching_refmultis_gfacs_mono.sh
#mkdir k_transcripts_matching_refmultis_gfacs_multi
sbatch k_transcripts_matching_refmultis_gfacs_multi.sh

echo "\nEnd time:"
date


