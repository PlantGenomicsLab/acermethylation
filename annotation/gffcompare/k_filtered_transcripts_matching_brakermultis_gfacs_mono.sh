#!/bin/bash
#SBATCH --job-name=gfacsk
#SBATCH -n 1
#SBATCH -c 4
#SBATCH -N 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=ALL
#SBATCH --mem=50G
#SBATCH --mail-user=susan.mcevoy@uconn.edu
#SBATCH -o gfacsk-%j.o
#SBATCH -e gfacsk-%j.e

module load perl/5.28.1

genome="/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta"
alignment="k_filtered_braker_multi_transcripts_gene_table.txt"
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"

perl "$script" \
-f gFACs_gene_table \
--statistics \
--statistics-at-every-step \
--rem-multiexonics \
--rem-genes-without-start-codon \
--rem-genes-without-stop-codon \
--create-gtf \
--create-gff3 \
--get-protein-fasta \
--fasta "$genome" \
-O k_filtered_transcripts_matching_brakermultis_gfacs_mono \
"$alignment"


