#!/bin/bash
#SBATCH --job-name=braker
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 36
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=300G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o braker_%j.out
#SBATCH -e braker_%j.err

hostname
echo "\nStart time:"
date

module load python/3.6.3
module load biopython/1.70
module load perl/5.28.1
module load bamtools/2.5.1
module load blast/2.10.0

tmpdir=$PWD/tmp
export PATH=/home/FCAM/smcevoy/BRAKER:/home/FCAM/smcevoy/BRAKER/scripts:$PATH
export AUGUSTUS_CONFIG_PATH=/home/FCAM/smcevoy/Augustus_3.4.0/config
export AUGUSTUS_BIN_PATH=/home/FCAM/smcevoy/Augustus_3.4.0/bin
export AUGUSTUS_SCRIPTS_PATH=/home/FCAM/smcevoy/Augustus_3.4.0/scripts
export PATH=/home/FCAM/smcevoy/Augustus_3.4.0/bin:/home/FCAM/smcevoy/Augustus_3.4.0/scripts:$PATH
export TMPDIR=$tmpdir
export BAMTOOLS_PATH=/isg/shared/apps/bamtools/2.5.1/bin
export BLAST_PATH=/isg/shared/apps/blast/ncbi-blast-2.10.0+/bin
export CDBTOOLS_PATH=/core/labs/Wegrzyn/annotationtool/software/cdbfasta
export SAMTOOLS_PATH=/isg/shared/apps/samtools/1.9/bin
export GENEMARK_PATH=~/gmes_linux_64
export DIAMOND_PATH=/core/labs/Wegrzyn/local_software/Oyster_River_Protocol/software/anaconda/install/envs/orp_busco/bin

species="acne2masked"
GENOME="/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta"
BAM="/core/projects/EBP/Wegrzyn/acer/acne/alignments/hisat/sorted_acne2_genome.bam"
wdir="/core/projects/EBP/Wegrzyn/acer/acne/annotation/braker/workingdirmasked2"

if [[ -d $AUGUSTUS_CONFIG_PATH/species/$species ]]; then rm -r $AUGUSTUS_CONFIG_PATH/species/$species; fi
cp ~/gm_key_64 ~/.gm_key

braker.pl --genome=${GENOME} --species=$species --softmasking --cores=36 --gff3 --bam=${BAM} --workingdir=$wdir/braker1/ 2> $wdir/braker1.log

echo "\nEnd time:"
date

