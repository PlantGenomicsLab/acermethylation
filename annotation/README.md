<h1>Gene Annotation</h1>

<h3>Alignments</h3>

Alignments generated for use in structural annotation described below.  

<h4>Hisat2</h4>  

basedir="/core/projects/EBP/Wegrzyn/acer/acne/assembly"  
genome="acne_pilon_ragtag.renamed.fasta"  

hisat2-build -p 30 -f "$basedir/$genome"  acne2_genome  

org="/labs/Wegrzyn/AcerGenomes/acne"  
trimmeddir="$org/transcriptome/reads/trimmed_reads"  
genomeidx="/core/projects/EBP/Wegrzyn/acer/acne/alignments/hisat/acne2_genome"  
f1=$trimmeddir"/trimmed_NEGU_1.fastq"  
f2=$trimmeddir"/trimmed_NEGU_2.fastq"  

hisat2 -x $genomeidx -1 $f1 -2 $f2 -p 32 -S acne2_genome.sam --dta  
samtools view -@ 32 -uhS acne2_genome.sam | samtools sort -@ 32 -m 3G -o sorted_acne2_genome.bam  

<h4>Gmap</h4>

Transcript (nucleotide) alignments were used in the gffCompare step below.  

indexdir=/core/projects/EBP/Wegrzyn/acer/acne/alignments/gmap  
genomepath=/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.fasta  
gmap_build -D $indexdir -d acne2_genome $genomepath  

idx="/core/projects/EBP/Wegrzyn/acer/acne/alignments/gmap"  
prefix="acne2"  
fasta="/labs/Wegrzyn/AcerGenomes/acne/transcriptome/transcripts/acne_transcriptome.fasta.filtered"  

gmap -a 1 --cross-species --fulllength -D "$idx" -d "$prefix"_genome -f gff3_gene "$fasta" --nthreads=16 --min-intronlength=9 --min-trimmed-coverage=0.95  --min-identity=0.95 -n1 > "$prefix"_gmap_okall.gff3 2> "$prefix"_gmap_okall.error   

genome="/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.fasta"  
alignment="acne2_gmap_okall.gff3"  
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"  

options="--statistics \  
--statistics-at-every-step \  
--allowed-inframe-stop-codons 0 \  
--unique-genes-only \  
--min-exon-size 6 \  
--min-intron-size 9 \  
--min-CDS-size 300 \  
--create-gtf"  

perl "$script" \  
-f gmap_2017_03_17_gff3 \  
"$options" \  
--fasta "$genome" \  
-O gfacs_o \  
"$alignment"  

<h4>GenomeThreader</h4>

Protein alignments were tested in Braker but not used for the final annotation.   

gt seqtransform -addstopaminos /labs/Wegrzyn/AcerGenomes/acne/transcriptome/transcripts/acne_transcriptome.faa.clustered > tmp  

gth -genomic ../../assembly/acne_pilon_ragtag.renamed.fasta -protein tmp -gff3out -startcodon -gcmincoverage 80 -finalstopcodon -introncutout -dpminexonlen 20 -skipalignmentout -o gth.aln -force -gcmaxgapwidth 1000000  

genome="../../assembly/acne_pilon_ragtag.renamed.fasta"  
alignment="gth.aln"  
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"  

perl "$script" \  
-f genomethreader_1.6.6_gff3 \  
--splice-rescue \  
--statistics \  
--statistics-at-every-step \  
--splice-table \  
--fasta "$genome" \  
-O gfacs_o \  
"$alignment"  

<h3>Structural annotation</h3>

<h4>Braker</h4>

Braker was used with the RNA alignments and the softmasked genome. The example command is listed here but see the braker.sh file above for how the run was set up.  

braker.pl --genome=${GENOME} --species=$species --softmasking --cores=36 --gff3 --bam=${BAM} --workingdir=$wdir/braker1/ 2> $wdir/braker1.log  

An alternate method, using Braker with protein (gmap) alignments and then combining the Braker-protein with Braker-rnaseq runs with TSEBRA, but it produced poor results in this situation.  

<h4>gFACs</h4>

gFACs was used to filter the gene model set in various ways  

The gfacs.sh script linked above steps through the general process. It calls filtergFACsGeneTable.py, also found above:  
 --unique-genes-only was set and they were split into mono and multiexonics.  Multiexonics missing both a start and stop codon, or with an exon smaller than 6bp, were removed. Monoexonics missing either a start or stop codon were removed and protein domains were verified with interproscan using the pfam database.  The mono and multiexonics remaining were recombined and checked for --allowed-inframe-stop-codons 0 --unique-genes-only (again, as a combined set).  

<h4>gffCompare</h4>

gffcompare was run to compare gene models produced by Braker with those from transript alignment with gmap.  The general idea is to find Braker gene models that are completely overlapped by a longer transcript-based gene model and swap in the transcript model for the braker one in the annotation. I also used transcripts as evidence to replace braker models that had been filtered out by criteria in gFACs.  

To do this, gffcompare was run twice, -o acne is the annotation filtered by gfacs.  -o acnebraker is the infiltered set directly from Braker.
#/core/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acne -s /core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta -r ../gfacs_filter_masked/final_o/out.gtf ../../alignments/gmap/gfacs_o/out_transcriptid.gtf  

/core/labs/Wegrzyn/local_software/gffcompare-0.11.5/gffcompare -o acnebraker -s /core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta -r ../braker/workingdirmasked2/braker1/augustus.hints.gtf ../../alignments/gmap/gfacs_o/out_transcriptid.gtf  

I then parsed this output to look for matches categorized as 'k' which is fully overlapping, or 'u' which is missing. gffcompare.sh and all this little parsing scripts are in the gffcompare folder above.  I did modifications to the gene model sets in a gfacs gene_table.txt file so the gfacs.sh in this folder was used to generate a new set of files from this modified gene set.  

<h3>Functional annotation</h3>

<h4>EnTAP</h4>
module load anaconda/2.4.0  
module load perl/5.30.1  
module load diamond/0.9.25  
module load interproscan/5.35-74.0  
module load TransDecoder/5.3.0  

/core/labs/Wegrzyn/EnTAP/EnTAP_v0.10.7/EnTAP/EnTAP --runP --ini /core/projects/EBP/Wegrzyn/acer/acne/annotation/entap/entap_config_0.10.7.ini  -i /core/projects/EBP/Wegrzyn/acer/acne/annotation/gffcompare/gfacs_o/genes_noasterisk.fasta.renamed.faa -d /isg/shared/databases/Diamond/Uniprot/uniprot_sprot.dmnd -d /isg/shared/databases/Diamond/RefSeq/complete.protein.faa.95.dmnd --threads 28 --out-dir /core/projects/EBP/Wegrzyn/acer/acne/annotation/entap/output  

<h3>Final suite of annotion files</h3>

Functional annotation can be used within gFACS to verify the structural set, but I kept all the filtered structural genes.  At this point, I renamed the genes and ran gFACS to create the final set of annotation-related files (.gtf, .gff3, .pep, .cds).   

genome="/core/projects/EBP/Wegrzyn/acer/acne/assembly/acne_pilon_ragtag.renamed.masked.fasta"  
alignment="../gffcompare/gfacs_o/out_renamed.gtf"  
script="/core/labs/Wegrzyn/gFACs/gFACs.pl"  

perl "$script" \  
-f gFACs_gtf \  
--no-processing \  
--statistics \  
--create-gff3 \  
--get-protein-fasta \  
--get-fasta \  
--fasta "$genome" \   
-O gfacs_o \  
"$alignment"  

