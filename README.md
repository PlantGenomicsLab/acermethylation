This repository contains detailed methods, commands, scripts, figures, and summary results for the following publication:  

Profiling genome-wide methylation in two maples: fine-scale approaches to detection with nanopore technology Susan L. McEvoy, Patrick G. S. Grady, Nicole Pauloski, Rachel J. O’Neill, Jill L. Wegrzyn  

Detailed methods for the methylation detection described in this manuscript are found in the 'methylation' folder above.  

Genome assembly, gene, and repeat annotation workflows are also detailed in their respective folders.  

The supplemental Excel file of summary statistics from each of these steps are found in the 'manuscript' folder.  It also contains figures published in the main text and as supplemental material.  

DOI 10.5281/zenodo.10659006  
