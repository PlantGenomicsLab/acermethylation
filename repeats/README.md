## Repeat annotations  

### De novo repeat identification   

A de novo repeat library was generated with RepeatModeler using the flag -LTRStruct to identify LTR elements.   

BuildDatabase -name acne2 ../acne_pilon_ragtag.renamed.fasta  
RepeatModeler -database acne2 --LTRStruct -pa 36  

### Repeat masking  

The de novo repeat library was combined with the curated plant database of LTRs, InpactorDB_redundant_final_V5.fasta for RepeatMasker.  

cat acne2-families.fa dbs/InpactorDB_non_redundant_final_V5.fasta > repeat_library_all.fasta  

RepeatMasker -lib repeat_library_all.fasta -pa 32 -gff -a -noisy -low -xsmall -dir outputfiles_dbs ../acne_pilon_ragtag.renamed.fasta  

### Parsing

ParseRM scripts were used to summarize the RepeatMasker .out file. (https://github.com/4ureliek/Parsing-RepeatMasker-Outputs). 
Additional short scripts (attached above) are included above that were used to reformat the sub-family naming of InpactorDB results so that they are included in appropriate family-level ParseRM totals.  
ParseRM -l was also used to create a file formated to create a landscape abundance plot in ggplot2.  

perl /core/labs/Wegrzyn/local_software/Parsing-RepeatMasker-Outputs/parseRM.pl -i ../outputfiles_dbs/acne_pilon_ragtag.renamed.fasta.out -p -f ../outputfiles_dbs/acne_pilon_ragtag.renamed.fasta -n -r ../repeat_library_all.fasta -l 50,1 -v  

Summary output is linked about as acne_pilon_ragtag.renamed.fasta.out.parseRM.summary.tab and acsa_pilon_ragtag.renamed.fasta.out.parseRM.summary.tab  
