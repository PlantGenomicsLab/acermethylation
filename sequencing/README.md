<h1>Sequencing</h1>  

<h2>ONT</h2>  

<h3>Contaminant filtering</h3>  

Index abvf contains archaea, bacteria, virus, fungi  
A short script related to filtering the reads based on the report is above: remove_contam_abvf_index.py   
centrifuge -p 25 -x abvf --report-file centrifuge_acne_abvf_index_report.tsv --quiet --min-hitlen 50 -q acne_superaccuracy_all.fastq  
#grep -vw "unclassified" centrifuge_3636036.out > acne_contam_reads_abvf_index.txt  
#awk NF=1 acne_contam_reads_abvf_index.txt > acne_contam_readids_abvf_index.txt  
#sort -u acne_contam_readids_abvf_index.txt > acne_contam_readids_abvf_index_uniq.txt  
python remove_contam_abvf_index.py  

<h3>Read length filtering</h3>  

Nanofilt was used to filter out reads less than 5k in length and NanoStat recalculated statistics.  

NanoFilt -l 5000 --logfile acnenanofilt.log ../centrifuge/acne_abvf_filtered.fastq > acne_abvf_5k_filtered.fastq  

NanoStat -o acne_nanoplot5k -n acne_nanoplot5k_summary.txt -t 20 --fastq acne_abvf_5k_filtered.fastq  

<h3>FAST5 filtering</h3>  

Get read ids so that we can fix the fast5 to match  

grep '^@' acne_abvf_5k_filtered.fastq > readids.txt  
Parse this for a list that is just readids > readidstrimmed.txt  

#ont_fast5_api has a command 'fast5_subset' to handle this.  https://github.com/nanoporetech/ont_fast5_api#fast5_subset  
fast5_subset -i /core/projects/EBP/Wegrzyn/acer/acne/reads/nanopore/2021APR12_McEvoy_AN-AA253-2013_PAG26470/20210412_1418_3G_PAG26470_5f2c462c/fast5_pass -s /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy_fast5_filtered/output -l /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy/nanofilt/readidstrimmed.txt  

Nanopolish uses multi_fast5 format, but DeepSignal requires single_fast5 so we need both   
This is another ont_fast5_api command   
multi_to_single_fast5 -i /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy_fast5_filtered/output -s /core/projects/EBP/Wegrzyn/acer/acne/reads/superaccuracy_single_fast5 -t 35  


<h2>Illumina</h2>

Raw Illumina reads were trimmed with FASTP   

fastp -i AN-B_S55_L002_R1_001.fastq.gz -I AN-B_S55_L002_R2_001.fastq.gz -o trimmed_AN-B_S55_L002_R1.fastq -O trimmed_AN-B_S55_L002_R2.fastq --unpaired1 trimmed_singles_AN-B_S55_L002.fastq --unpaired2 trimmed_singles_AN-B_S55_L002.fastq -q 30 -l 100   
