General summary:         
Mean read length:                 11,322.3
Mean read quality:                    10.5
Median read length:               10,381.0
Median read quality:                  10.5
Number of reads:               4,286,423.0
Read length N50:                  16,265.0
STDEV read length:                 8,409.6
Total bases:              48,532,191,821.0
Number, percentage and megabases of reads above quality cutoffs
>Q5:	4286423 (100.0%) 48532.2Mb
>Q7:	4286405 (100.0%) 48532.2Mb
>Q10:	2504433 (58.4%) 28230.1Mb
>Q12:	973871 (22.7%) 10448.8Mb
>Q15:	60645 (1.4%) 462.8Mb
Top 5 highest mean basecall quality scores and their read lengths
1:	21.6 (533)
2:	21.2 (216)
3:	21.1 (202)
4:	20.9 (445)
5:	20.9 (1226)
Top 5 longest reads and their mean basecall quality score
1:	620305 (7.8)
2:	536744 (7.3)
3:	520060 (8.1)
4:	505962 (7.9)
5:	496430 (7.3)
