#!/bin/bash
#SBATCH --job-name=deepsignalplant1
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
###SBATCH -w xanadu-02
#SBATCH --mail-type=ALL
#SBATCH --mem=300G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o run_deepsignal1_%j.out
#SBATCH -e run_deepsignal1_%j.err

hostname
echo "\nStart time:"
date

module load gcc/10.2.0

# once the appropriate conda is initialized, activate the environment you want
# deepsignal environment
conda activate meteore_deepsignalplant_env1

# per site output
snakemake -s Deepsignalplant1 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-prob.tsv --cores 36
snakemake -s Deepsignalplant1 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG-raw.tsv

python /core/projects/EBP/software/envs/meteore/envs/meteore_deepsignalplant_env1/scripts/split_freq_file_by_5mC_motif_sm.py --freqfile deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG-raw.tsv

echo "\nEnd time:"
date

