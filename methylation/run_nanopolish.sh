#!/bin/bash
#SBATCH --job-name=run_nanopolish
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 32
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o run_nanopolish_%j.out
#SBATCH -e run_nanopolish_%j.err

hostname
echo "\nStart time:"
date

module load gcc/10.2.0

# once the appropriate conda is initialized, activate the environment you want
# nanopolish environment
conda activate meteore_nanopolish_env

# issue with the conda version of samtools/1.7 but using our module works
# notes on error: https://github.com/riboviz/riboviz/issues/335
# this has to be kept after the conda activate command to use the correct one
module load samtools/1.7

# per site results
snakemake -s Nanopolish nanopolish_results/acne_abvf_5k_filtered_nanopolish-freq-perCG.tsv --cores all

# per read results - formats output for combined model usage
snakemake -s Nanopolish nanopolish_results/acne_abvf_5k_filtered_nanopolish-perRead-score.tsv --cores all

echo "\nEnd time:"
date

