<h1>Methylation Detection of Plants with METEORE</h1>

Methylation detection is handled by the snakemake workflows and R scripts of the METEORE workflow pipeline.  We have selected Nanopolish and Deepsignal to create a consensus of CG methylation within METEORE.  METEORE comes with DeepSignal which uses an animal-based model, so scripts have been modified to work with DeepSignal-Plant.  This will also allow us to output CHG and CHH. While it is not a consensus as these contexts aren't available in Nanopolish, but the output formatting scripts will allow inclusion of these alongside CG in comparative analysis.   

Follow the instructions in the METEORE git for installing the various conda environments, how to set up the directory, and for the general workflow.  
https://github.com/comprna/METEORE  
The notes below and scripts linked above document this process for the Acer including modifications needed.  

<h3>Prepare reads for Nanopolish</h3>

The following steps are specific to Nanopolish and aren't required to run Deepsignal-Plant.  

I am using rebasecalled reads, so need to annotate the fast5 with the new fastq.  I'm adding them to group Basecall_1D_000.   
tombo preprocess annotate_raw_with_fastqs --fast5-basedir data/superaccuracy_single_fast5 \   
--fastq-filenames acsa_abvf_5k_filtered.fastq \   
--basecall-group Basecall_1D_000 --basecall-subgroup BaseCalled_template \   
--overwrite --processes 35   

Using the fastq annotated to Basecall_1D_000, we then resquiggle the reads.  These are written them to RawGenomeCorrected_000 for the resquiggle to the original genome and RawGenomeCorrected_001 to the new genome.  Signal-align-parameters were used for the original genome, but defaults were used for the new genomes. The parameter adjustment was meant to help with error "Read event to sequence alignment extends beyond bandwidth"  

original genome:  
(tombo resquiggle --ignore-read-locks --signal-align-parameters 4.2 4.2 2000 2500 20.0 40 750 2500 250 --dna data/superaccuracy_single_fast5 data/acsa_genome.fasta   --processes 35  --corrected-group RawGenomeCorrected_000 \   
--basecall-group Basecall_1D_000 --overwrite   

new genome (note the different --corrected group:  
tombo resquiggle --ignore-read-locks --dna data/superaccuracy_single_fast5 data/acsa_pilon_ragtag.renamed.fasta --processes 36 --corrected-group RawGenomeCorrected_001 --basecall-group Basecall_1D_000   

<h3>Run Nanopolish</h3>  

The file run_nanopolish.sh steps through the Nanopolish workflow (both files above).  
Modifications to this step were minimal.  I commented out the rule that runs a minimap alignment and ran it outside of METEORE because the version of minimap loaded by the conda environment meteore_nanopolish_env is old and we had a much newer version installed on the HPC.  

<h3>Run DeepSignal-Plant, step 1</h3>  

METEORE scripts break down DeepSignal-Plant into two steps, the first is to call modifications and the second is formatting results   

See files run_deepsignalplant1.sh and Deepsignalplant1 above, details of which are described below:  

<h4>Calling modifications</h4>   

This can be done one of two ways, from extracted features, as shown in METEORE git documentation, or directly from the reads, which is the approach recommended by Deepsignal-plant.  I ran the former method first, for the original acne only, and but later re-ran the orignal and new genomes using the latter method.   

<h5>Calling modifications from reads</h5>   

snakemake -s Deepsignalplant1 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-prob.tsv --cores 36  
snakemake -s Deepsignalplant1 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG-raw.tsv  

For the shell command below:  
  --motifs C flag is required to get all contexts (CG, CHG, CHH)   
  --corrected_group RawGenomeCorrected_000 (set in the resquiggle command above)specified to ensure we get the resquiggled reads placed in this slot with the tombo resquiggle command     
  --corrected_group RawGenomeCorrected_001 was used for v2 genomes   

rule call_modification:  
    input:  
        "data/{sample}",  
    output:  
        "deepsignalplant_results/{sample}_deepsignal-prob.tsv"  
    shell:  
        "CUDA_VISIBLE_DEVICES=0 deepsignal_plant call_mods --input_path {input} --nproc 30 --model_path data/   model.dp2.CNN.arabnrice2-1_120m_R9.4plus_tem.bn13_sn16.both_bilstm.epoch6.ckpt --result_file {output} --nproc_gpu 6 --motifs C --corrected_group  RawGenomeCorrected_000 --reference_path data/acne_genome.fasta"   

Sample output:  
acne_105        25985899        -       1530729 ac499f1f-8bcb-4f18-baae-b64eea569655    t       0.999922        7.8e-05 0       TGCAT   

<h5>Calling modifications from feature - not used, just here for documentation</h5>  

The extract_feature rule was removed from the Deepsignalplant1 file above.  

rule extract_feature:  
    input:  
        fast5="data/{sample}",  
        ref="data/acne_genome.fasta"  
    output:  
        "deepsignalplant_results/{sample}_deepsignal-feature.tsv"  
    shell:  
        "deepsignal_plant extract --fast5_dir {input.fast5} --reference_path {input.ref} --is_dna true --write_path {output} --nproc 36"    

rule call_modification:  
    input:  
        "deepsignalplant_results/{sample}_deepsignal-feature.tsv",   
    output:  
        "deepsignalplant_results/{sample}_deepsignal-prob.tsv"   
    shell:  
        "CUDA_VISIBLE_DEVICES=0 deepsignal_plant call_mods --input_path {input} --nproc 30 --model_path data/  model.dp2.CNN.arabnrice2-1_120m_R9.4plus_tem.bn13_sn16.both_bilstm/model.dp2.CNN.arabnrice2-1_120m_R9.4plus_tem.bn13_sn16.both_bilstm --result_file {output}  --nproc_gpu 6"   
  
Sample output:   
-feature.tsv  
acne_102        5405680 -       28406850        ce5ce342-a587-48b4-80fb-6f3539dd796f    t       TACCATCGTAAGT   1.115502,0.181593,0.909048,0.192711,-0.420259,1.050648,0.233477,-0.638171,0.985793,-0.127026,-0.879431,-1.834464,0.837924       0.14209,0.23024,0.130244,0.208624,0.117055,0.255827,0.19239,0.371105,0.225795,0.180291,0.125195,0.181971,0.136312       21,13,24,28,20,10,8,5,33,29,10,14,10    1.141444,0.985793,1.193328,1.193328,1.115502,1.141444,1.167386,1.374921,1.141444,1.21927,0.882025,0.856083,1.141444,1.011735,0.985793,0.856083;0.0,0.622606,0.648548,0.155651,0.103768,-0.051884,0.311303,0.12971,0.259419,0.155651,0.181593,0.025942,0.0,-0.181593,0.0,0.0;0.570722,1.011735,0.933909,0.752315,0.804199,0.959851,0.856083,0.726374,0.700432,0.933909,1.08956,0.959851,0.959851,0.933909,0.907967,0.985793;0.233477,0.051884,0.259419,0.077826,0.155651,0.12971,0.025942,-0.077826,0.259419,0.337245,0.233477,0.389129,0.103768,0.389129,0.207535,0.025942;-0.570722,-0.363187,-0.441013,-0.466954,-0.259419,-0.259419,-0.207535,-0.466954,-0.67449,-0.466954,-0.311303,-0.441013,-0.466954,-0.54478,-0.363187,-0.415071;0.0,0.0,0.0,0.415071,1.011735,1.193328,0.933909,1.115502,1.141444,1.374921,1.141444,0.882025,1.297096,0.0,0.0,0.0;0.0,0.0,0.0,0.0,0.648548,0.389129,0.12971,0.0,0.051884,0.207535,0.233477,0.207535,0.0,0.0,0.0,0.0;0.0,0.0,0.0,0.0,0.0,-0.051884,-0.415071,-0.726374,-0.882025,-1.115502,0.0,0.0,0.0,0.0,0.0,0.0;0.54478,0.778257,0.67449,1.037677,0.441013,1.323038,0.67449,0.856083,0.959851,1.297096,1.297096,0.985793,0.959851,1.271154,1.193328,1.063618;0.0,-0.155651,-0.051884,-0.259419,0.12971,-0.12971,-0.103768,-0.259419,0.0,-0.103768,-0.077826,-0.466954,-0.441013,-0.259419,-0.077826,-0.389129;0.0,0.0,0.0,-0.804199,-1.08956,-0.778257,-0.804199,-0.752315,-0.907967,-0.726374,-0.882025,-0.959851,-1.08956,0.0,0.0,0.0;0.0,-2.127237,-1.867818,-1.815934,-1.841876,-1.76405,-1.815934,-1.76405,-1.919702,-1.738108,-1.945644,-   1.945644,-1.815934,-2.023469,-1.297096,0.0;0.0,0.0,0.0,0.752315,0.67449,0.907967,0.778257,0.907967,1.063618,0.622606,0.804199,0.830141,1.037677,0.0,0.0,0.0   

-prob.tsv # contains the raw per-read results including the position of the CpG site, read ID, strand, methylated probability, unmethylated probability etc.   
acne_102        5405680 -       28406850        ce5ce342-a587-48b4-80fb-6f3539dd796f    t       0.006824        0.993176        1       ATCGT    

<h4>Update to calculate_frequency rule:</h4>  

The original Meteore snakefile uses a Python script call_modification_frequency.py provided by DeepSignal. I changed mine to use the current Deepsignal-plant command 'call_freq' ('.  Output formatting seems the same.   

rule calculate_frequency:  
    input:  
        "deepsignalplant_results/{sample}_deepsignal-prob.tsv"   
    output:  
        "deepsignalplant_results/{sample}_deepsignal-freq-perCG-raw.tsv"   
    shell:   
        "deepsignal_plant call_freq --input_path {input} --result_file {output}"     

Sample output:   
acne_105        25985713        -       1530915 16.656  4.344   2       19      21      0.0952  TCCGG    

<h3>Split results by motif</h3>  

This step is not in the original Meteore and is a script provided by Deepsignal-plant.  It will allow us to analyze the results by context.  I modified it to split the results even further, by all possible sequence contexts, not just CG, CHG, and CHH.  See above: split_freq_file_by_5mC_motif_sm.py   

python /core/projects/EBP/software/envs/meteore/envs/meteore_deepsignalplant_env1/scripts/split_freq_file_by_5mC_motif_sm.py --freqfile deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG-raw.tsv   

Outputs 15 files:   
ssuperaccuracy_single_fast5_deepsignal-freq-perCG-raw.CAA.tsv   
superaccuracy_single_fast5_deepsignal-freq-perCG-raw.CAC.tsv   
superaccuracy_single_fast5_deepsignal-freq-perCG-raw.CAG.tsv  
superaccuracy_single_fast5_deepsignal-freq-perCG-raw.CAT.tsv   
superaccuracy_single_fast5_deepsignal-freq-perCG-raw.CCA.tsv   
and so on...   

Sample output:  
acne_105        25985922        -       1530706 35.993  0.007   0       36      36      0.0000  TTCAA   

<h3>Run DeepSignal-Plant, step 2</h3>  

Overview: https://github.com/comprna/METEORE/tree/master?tab=readme-ov-file#produce-result-files-that-are-in-the-same-format-generated-by-other-tools   

In Deepsignalplant2, the following rule was modified so that it would run on all 3 motif files generated by the previous step.  
It is reformatting the freq-perCG-raw files as augmented BED files   

snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CG.tsv --cores 30  
snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CHG.tsv --cores 30  
snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CHH.tsv --cores 30   

rule combine_sites_CG:  
    input:  
        freq="deepsignalplant_results/{sample}_deepsignal-freq-perCG-raw.CG.tsv",  
        script="/core/projects/EBP/software/meteore/METEORE/script_in_snakemake/run_deepsignal.R"  
    output:   
        file1="deepsignalplant_results/{sample}_deepsignal-freq-perCG.CG.tsv",  
        file2="deepsignalplant_results/{sample}_deepsignal-freq-perCG-combStrand.CG.tsv"   
    shell:   
        "Rscript {input.script} {input.freq} {output.file1} {output.file2}"   

rule combine_sites_CHG:   
    input:   
        freq="deepsignalplant_results/{sample}_deepsignal-freq-perCG-raw.CHG.tsv",   
        script="/core/projects/EBP/software/meteore/METEORE/script_in_snakemake/run_deepsignal.R"   
    output:   
        file1="deepsignalplant_results/{sample}_deepsignal-freq-perCG.CHG.tsv",   
        file2="deepsignalplant_results/{sample}_deepsignal-freq-perCG-combStrand.CHG.tsv"   
    shell:   
        "Rscript {input.script} {input.freq} {output.file1} {output.file2}"   

rule combine_sites_CHH:   
    input:   
        freq="deepsignalplant_results/{sample}_deepsignal-freq-perCG-raw.CHH.tsv",   
        script="/core/projects/EBP/software/meteore/METEORE/script_in_snakemake/run_deepsignal.R"   
    output:   
        file1="deepsignalplant_results/{sample}_deepsignal-freq-perCG.CHH.tsv",   
        file2="deepsignalplant_results/{sample}_deepsignal-freq-perCG-combStrand.CHH.tsv"   
    shell:   
        "Rscript {input.script} {input.freq} {output.file1} {output.file2}"   


Sample output (2 files for this one):   

-freq-perCG.CG.tsv   
Chr     Pos_start       Pos_end Coverage        Methylation     Strand    
acne_102         5405680         5405681        37      0.9459  -   

-freq-perCG-combStrand.CG.tsv  
Chr     Pos_start       Pos_end Coverage        Methylation   
acne_102         5405679         5405680        65      0.9194  

<h4>Separate out CGs for consensus steps</h4>

The next command goes back several steps using the call_modifications output as input.  This step is formatting the data for input to the consensus step.  Since consensus only works on CG, we don't want to include all the motifs.  I added a step not originally in Meteore to split the -prob.tsv file by motif so that we can move forward with only CG.   
Note that this is the original split_freq_file_by_5mC_motif.py, not the one I modified (split_freq_file_by_5mC_motif_sm.py)   

python /core/projects/EBP/software/envs/meteore/envs/meteore_deepsignalplant_env1/scripts/split_freq_file_by_5mC_motif.py --freqfile deepsignalplant_results/superaccuracy_singlpsignal-prob.tsv   

<h4>Per-read output for combined model usage</h4>  

snakemake -s Deepsignalplant2secondpart deepsignalplant_results/superaccuracy_single_fast5_deepsignal-perRead-score_fixed.tsv --cores 1  

Deepsignalplant2secondpart calls and R file I modified.  There were situations where division by 0s was returning Inf resulting in errors on the following step.  

rule create_input_for_comb_model_CG:  
    input:   
        prob="deepsignalplant_results/{sample}_deepsignal-prob.CG.tsv",  
        script="/core/projects/EBP/software/meteore/METEORE/script_in_snakemake/format_deepsignal.R"  
    output:   
        "deepsignalplant_results/{sample}_deepsignal-perRead-score.tsv"   
    shell:   
        "Rscript {input.script} {input.prob} {output}"   

format_deepsignal_sm.R (linked above) was changed to remove anything exactly zero or exactly 1.  Column 'unmethylated' zeros were the main problem.   

df <- df %>% select(1,2,3,5,7,8) # chr, pos, strand, id, unmethylated, methylated  

Added the following 4 lines to remove 0s and 1s.  The 0s were returning Inf in the log2  
df["V8"][df["V7"] == "0"] <- 0.9999991  
df["V7"][df["V7"] == "0"] <- 0.0000009  
df["V7"][df["V8"] == "0"] <- 0.9999991  
df["V8"][df["V8"] == "0"] <- 0.0000009  

Sample output:   
ID      Chr     Pos     Strand  Score   
ac499f1f-8bcb-4f18-baae-b64eea569655    acne_105        25985714        -       -4.12174053254857   

<h4>More formatting combined site frequencies for additional subcontext</h4>  
This rule is the same as was shown in the first step of DeepSignal part 2, but just fixing formatting on the remaining subcontexts   

snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAG.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCG.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTG.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAA.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAC.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAT.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCA.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCC.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCT.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTA.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTC.tsv --cores 30  
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTT.tsv --cores 30  

<h3>Combined model usage</h3>  

Random forest steps as descibed here: https://github.com/comprna/METEORE/tree/master?tab=readme-ov-file#combined-model-random-forest-usage   

<h4>Combining both NanoPolish and DeepSignal-Plant</h4>

See run_randomforest.sh above - both default and optimized settings were tried   

python /core/projects/EBP/software/meteore/METEORE/combination_model_prediction.py -i model_content.tsv -m default -o acne2_deepsignal_nanopolish-default-model-perRead.tsv   

python /core/projects/EBP/software/meteore/METEORE/combination_model_prediction.py -i model_content.tsv -m optimized -o acne2_deepsignal_nanopolish-optimized-model-perRead.tsv  

<h4>Final combined, formatted, CG frequency results</h4>  

See persite.sh above  

both the default and optimize tests  

python /core/projects/EBP/software/meteore/METEORE/prediction_to_mod_frequency.py -i combined_model_results/acne_deepsignal_nanopolish-default-model-perRead.tsv -t 0.5 -o combined_model_results/acne_deepsignal_nanopolish-per-site-freq-perCG.tsv  

python /core/projects/EBP/software/meteore/METEORE/prediction_to_mod_frequency.py -i combined_model_results/acne2_deepsignal_nanopolish-optimized-model-perRead.tsv -t 0.5 -o combined_model_results/acne2_deepsignal_nanopolish-optimized-per-site-freq-perCG.tsv  
