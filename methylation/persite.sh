#!/bin/bash
#SBATCH --job-name=persite
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=general
#SBATCH --qos=general
#SBATCH --mail-type=END
#SBATCH --mem=240G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o persite_%j.out
#SBATCH -e persite_%j.err

hostname
echo "\nStart time:"
date

module load gcc/10.2.0

#python /core/projects/EBP/software/meteore/METEORE/prediction_to_mod_frequency.py -i combined_model_results/acne_deepsignal_nanopolish-default-model-perRead.tsv -t 0.5 -o combined_model_results/acne_deepsignal_nanopolish-per-site-freq-perCG.tsv

python /core/projects/EBP/software/meteore/METEORE/prediction_to_mod_frequency.py -i combined_model_results/acne2_deepsignal_nanopolish-optimized-model-perRead.tsv -t 0.5 -o combined_model_results/acne2_deepsignal_nanopolish-optimized-per-site-freq-perCG.tsv

echo "\nEnd time:"
date

