#!/bin/bash
#SBATCH --job-name=run_randomforest
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=400G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o run_randomforest_%j.out
#SBATCH -e run_randomforest_%j.err

hostname
echo "\nStart time:"
date

module load gcc/10.2.0

conda activate python3.6.10

python /core/projects/EBP/software/meteore/METEORE/combination_model_prediction.py -i model_content.tsv -m default -o acne2_deepsignal_nanopolish-default-model-perRead.tsv

python /core/projects/EBP/software/meteore/METEORE/combination_model_prediction.py -i model_content.tsv -m optimized -o acne2_deepsignal_nanopolish-optimized-model-perRead.tsv

echo "\nEnd time:"
date

