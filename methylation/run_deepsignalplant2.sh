#!/bin/bash
#SBATCH --job-name=run_deepsignal2
#SBATCH -N 1
#SBATCH -n 1
#SBATCH -c 1
#SBATCH --partition=himem
#SBATCH --qos=himem
#SBATCH --mail-type=END
#SBATCH --mem=500G
#SBATCH --mail-user=susanlmcevoy@gmail.com
#SBATCH -o run_deepsignal2_%j.out
#SBATCH -e run_deepsignal2_%j.err

hostname
echo "\nStart time:"
date

module load gcc/10.2.0

# deepsignal step 2 enironment
conda activate meteore_deepsignal_env2

# similar to deepsignal1 but reformatted as augmented BED file
# necessary for the next command
snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CG.tsv --cores 30
snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CHG.tsv --cores 30
snakemake -s Deepsignalplant2 deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CHH.tsv --cores 30

python /core/projects/EBP/software/envs/meteore/envs/meteore_deepsignalplant_env1/scripts/split_freq_file_by_5mC_motif.py --freqfile deepsignalplant_results/superaccuracy_single_fast5_deepsignal-prob.tsv

# per-read output for combined model usage
snakemake -s Deepsignalplant2secondpart deepsignalplant_results/superaccuracy_single_fast5_deepsignal-perRead-score_fixed.tsv --cores 1

snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAG.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCG.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTG.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAA.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAC.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CAT.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCA.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCC.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CCT.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTA.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTC.tsv --cores 30
snakemake -s Deepsignalplant2subcontexts deepsignalplant_results/superaccuracy_single_fast5_deepsignal-freq-perCG.CTT.tsv --cores 30

echo "\nEnd time:"
date

